export enum EToken {
	INTEGER, // 0
	PLUS, // 1
	MINUS,// 2
	MUL,// 3
	DIV,// 4
	LPARAN,// 5
	RPARAN,// 6
	FACT,// 7
	POWER,// 8
	EOF// 9
}

export class Token {
	public value: number;
	public type: EToken;

	constructor(type: EToken, value?: number) {
		this.type = type;
		this.value = value ? value : 0;
	}
}