import Lexer from './lexer';
import { Token, EToken } from './token';

export class UniOp {
	public mid: any;
	public token: Token;

	constructor(mid: any, token: Token) {
		this.mid = mid;
		this.token = token;
	}
}

export class BinOp {
	public left: any;
	public right: any;
	public token: Token;

	constructor(left: any, token: Token, right: any,) {
		this.left = left;
		this.right = right;
		this.token = token;
	}
}


export class Num {
	public token: Token;
	public value: number;

	constructor(token: Token) {
		this.token = token;
		this.value = token.value;
	}
}

export class Parser {
	private lexer: Lexer;
	private current_token: Token;

	constructor(lexer: Lexer) {
		this.lexer = lexer;
		this.current_token = this.lexer.get_next_token();
	}

	private eat(token_type: EToken) : void {
		if (this.current_token.type == token_type) {
			this.current_token = this.lexer.get_next_token();
		} else {
			throw SyntaxError("Invalid syntax!");
		}
	}

	private factor() {
		const token = this.current_token;

		if (token.type == EToken.INTEGER) {
			this.eat(EToken.INTEGER);
			return new Num(token);
		} else if (token.type == EToken.LPARAN) {
			this.eat(EToken.LPARAN);
			const node = this.expr();
			this.eat(EToken.RPARAN);
			return node;
		} else if (token.type == EToken.FACT) {
			this.eat(EToken.FACT);
			const node = this.expr();
			return new UniOp(node, token);
		} else {
			throw SyntaxError("Factor bad syntax!");
		}
	}

	private term() {
		let node: any = this.factor();

		while (
			this.current_token.type == EToken.MUL ||
			this.current_token.type == EToken.DIV ||
			this.current_token.type == EToken.POWER
			) {
			const token = this.current_token;
			if (token.type == EToken.MUL) {
				this.eat(EToken.MUL);
			} else if (token.type == EToken.DIV) {
				this.eat(EToken.DIV);
			} else if (token.type == EToken.POWER) {
				this.eat(EToken.POWER);
			}

			node = new BinOp(node, token, this.factor());
		}

		return node;
	}

	private expr() {
		let node = this.term()

		while (this.current_token.type == EToken.PLUS || this.current_token.type == EToken.MINUS) {
			const token = this.current_token;
			if (token.type == EToken.PLUS) {
				this.eat(EToken.PLUS);
			} else if (token.type == EToken.MINUS) {
				this.eat(EToken.MINUS);
			}

			node = new BinOp(node, token, this.term());
		}

		return node;
	}

	public parse() {
		return this.expr();
	}
}