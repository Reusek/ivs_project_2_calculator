import '@testing-library/jest-dom';

import { Token, EToken } from '../lib/interpreter/token';
import Lexer from '../lib/interpreter/lexer';
<<<<<<< HEAD
import { Parser, BinOp, Num } from '../lib/interpreter/parser';
import { Interpreter } from '../lib/interpreter/interpreter';
=======
>>>>>>> origin/UI

/**
 * To run all the tests use:
 * 		yarn test
 * 
 * To run this test file use:
 * 		yarn test src/__tests__/Interpreter.test.tsx
 * 
 * To run specific tests use:
 * 		yarn test -t [keyword to match]
 *
<<<<<<< HEAD
 * 	example:	yarn test -t lexer
 * 				- runs Lexer tests
 * 
 * 				yarn test -t parser
 * 				- runs Parser tests
=======
 * 	example:	yarn test -t sub
 * 				- runs sub(a, b) tests
 *
 * 				yarn test -t pow
 * 				(or: yarn test -t power)
 * 				- runs power(a, b) tests
>>>>>>> origin/UI
 *
 * (run yarn from ../ivs_project_2_calculator/src folder)
 *
 */


describe('Lexer tests', () => {
	describe('One token', () => {
		test.each`
			str 	| expected
			${"5"}	| ${{ type: EToken.INTEGER, value: 5 }}
			${"420"}| ${{ type: EToken.INTEGER, value: 420 }}
			${"+"}	| ${{ type: EToken.PLUS, value: 0}}
			${"-"}	| ${{ type: EToken.MINUS, value: 0}}
			${"*"}	| ${{ type: EToken.MUL, value: 0}}
			${"/"}	| ${{ type: EToken.DIV, value: 0}}
			${"("}	| ${{ type: EToken.LPARAN, value: 0}}
			${")"}	| ${{ type: EToken.RPARAN, value: 0}}
		`('Lexer.get_next_token() with string "$str" should return $expected', ({ str, expected }) => {
			const l = new Lexer(str);
			expect(l.get_next_token()).toEqual(expected);
			expect(l.get_next_token()).toEqual({ type: EToken.EOF, value: 0 })
		});
	});

	describe('String / multiple tokens', () => {
		describe('(1 + 1) * 20 + 0', () => {
			const l = new Lexer("(1 + 1) * 20 + 0");
			test.each`
				char 					| expected
				${l.get_next_token()}	| ${{ type: EToken.LPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 1 }}
				${l.get_next_token()}	| ${{ type: EToken.PLUS, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 1 }}
				${l.get_next_token()}	| ${{ type: EToken.RPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.MUL, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 20 }}
				${l.get_next_token()}	| ${{ type: EToken.PLUS, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 0 }}
<<<<<<< HEAD
				${l.get_next_token()}	| ${{ type: EToken.EOF, value: 0 }}
=======
>>>>>>> origin/UI
			`('Lexer.get_next_token() - got $char, should return $expected', ({ char, expected }) => {
				expect(char).toEqual(expected);
			});
		});
		
		describe('1 2 35', () => {
			const l = new Lexer("1 2 35");
<<<<<<< HEAD
			/// TODO: SyntaxError or 1235 (?)
			test.each`
				char 					| expected
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 1235 }}
				${l.get_next_token()}	| ${{ type: EToken.EOF, value: 0 }}
=======
			/* Not sure if desired behavior (?) */
			test.each`
				char 					| expected
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 1 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 2 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 35 }}
			`('Lexer.get_next_token() - got $char, should return $expected', ({ char, expected }) => {
				expect(char).toEqual(expected);
			});
		});

		describe('((1)2 3) 45', () => {
			const l = new Lexer("((1)2 3) 45");
			test.each`
				char 					| expected
				${l.get_next_token()}	| ${{ type: EToken.LPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.LPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 1 }}
				${l.get_next_token()}	| ${{ type: EToken.RPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 2 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 3 }}
				${l.get_next_token()}	| ${{ type: EToken.RPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 45 }}
			`('Lexer.get_next_token() - got $char, should return $expected', ({ char, expected }) => {
				expect(char).toEqual(expected);
			});
		});

		describe('42+-*/(22)45', () => {
			const l = new Lexer("42+-*/(22)45");
			test.each`
				char 					| expected
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 42 }}
				${l.get_next_token()}	| ${{ type: EToken.PLUS, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.MINUS, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.MUL, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.DIV, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.LPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 22 }}
				${l.get_next_token()}	| ${{ type: EToken.RPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 45 }}
			`('Lexer.get_next_token() - got $char, should return $expected', ({ char, expected }) => {
				expect(char).toEqual(expected);
			});
		});

		describe('1   ()       +          2   - 45 ) 20', () => {
			const l = new Lexer("1   ()       +          2   - 45 ) 20");
			test.each`
				char 					| expected
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 1 }}
				${l.get_next_token()}	| ${{ type: EToken.LPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.RPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.PLUS, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 2 }}
				${l.get_next_token()}	| ${{ type: EToken.MINUS, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 45 }}
				${l.get_next_token()}	| ${{ type: EToken.RPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 20 }}
			`('Lexer.get_next_token() - got $char, should return $expected', ({ char, expected }) => {
				expect(char).toEqual(expected);
			});
		});

		describe('1(2))3(4 5 6 ++ 7 // - 8)(', () => {
			const l = new Lexer("1(2))3(4 5 6 ++ 7 // - 8)(");
			test.each`
				char 					| expected
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 1 }}
				${l.get_next_token()}	| ${{ type: EToken.LPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 2 }}
				${l.get_next_token()}	| ${{ type: EToken.RPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.RPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 3 }}
				${l.get_next_token()}	| ${{ type: EToken.LPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 4 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 5 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 6 }}
				${l.get_next_token()}	| ${{ type: EToken.PLUS, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.PLUS, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 7 }}
				${l.get_next_token()}	| ${{ type: EToken.DIV, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.DIV, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.MINUS, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 8 }}
				${l.get_next_token()}	| ${{ type: EToken.RPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.LPARAN, value: 0 }}
			`('Lexer.get_next_token() - got $char, should return $expected', ({ char, expected }) => {
				expect(char).toEqual(expected);
			});
		});


		describe('(20 + ) 30 - 20)', () => {
			const l = new Lexer("(20 + ) 30 - 20)");
			test.each`
				char 					| expected
				${l.get_next_token()}	| ${{ type: EToken.LPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 20 }}
				${l.get_next_token()}	| ${{ type: EToken.PLUS, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.RPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 30 }}
				${l.get_next_token()}	| ${{ type: EToken.MINUS, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 20 }}
				${l.get_next_token()}	| ${{ type: EToken.RPARAN, value: 0 }}
>>>>>>> origin/UI
			`('Lexer.get_next_token() - got $char, should return $expected', ({ char, expected }) => {
				expect(char).toEqual(expected);
			});
		});

		describe('(123) + 456', () => {
			const l = new Lexer("(123) + 456");
			test.each`
				char 					| expected
				${l.get_next_token()}	| ${{ type: EToken.LPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 123 }}
				${l.get_next_token()}	| ${{ type: EToken.RPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.PLUS, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 456 }}
<<<<<<< HEAD
				${l.get_next_token()}	| ${{ type: EToken.EOF, value: 0 }}
=======
			`('Lexer.get_next_token() - got $char, should return $expected', ({ char, expected }) => {
				expect(char).toEqual(expected);
			});
		});

		describe('((/ 123 * 150 051 + (97 5)*', () => {
			const l = new Lexer("((/ 123 * 150 051 + (97 5)*");
			test.each`
				char 					| expected
				${l.get_next_token()}	| ${{ type: EToken.LPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.LPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.DIV, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 123 }}
				${l.get_next_token()}	| ${{ type: EToken.MUL, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 150 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 51 }}
				${l.get_next_token()}	| ${{ type: EToken.PLUS, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.LPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 97 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 5 }}
				${l.get_next_token()}	| ${{ type: EToken.RPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.MUL, value: 0 }}
>>>>>>> origin/UI
			`('Lexer.get_next_token() - got $char, should return $expected', ({ char, expected }) => {
				expect(char).toEqual(expected);
			});
		});

		describe('2748951398 * 5716978421787', () => {
			const l = new Lexer("2748951398 * 5716978421787");
			test.each`
				char 					| expected
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 2748951398 }}
				${l.get_next_token()}	| ${{ type: EToken.MUL, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 5716978421787 }}
<<<<<<< HEAD
				${l.get_next_token()}	| ${{ type: EToken.EOF, value: 0 }}
=======
>>>>>>> origin/UI
			`('Lexer.get_next_token() - got $char, should return $expected', ({ char, expected }) => {
				expect(char).toEqual(expected);
			});
		});

		describe('((231 + 63) * 155) / (242 * (120 - 8445))', () => {
			const l = new Lexer("((231 + 63) * 155)/ (242*(120-8445))");
			test.each`
				char 					| expected
				${l.get_next_token()}	| ${{ type: EToken.LPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.LPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 231 }}
				${l.get_next_token()}	| ${{ type: EToken.PLUS, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 63 }}
				${l.get_next_token()}	| ${{ type: EToken.RPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.MUL, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 155 }}
				${l.get_next_token()}	| ${{ type: EToken.RPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.DIV, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.LPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 242 }}
				${l.get_next_token()}	| ${{ type: EToken.MUL, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.LPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 120 }}
				${l.get_next_token()}	| ${{ type: EToken.MINUS, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 8445 }}
				${l.get_next_token()}	| ${{ type: EToken.RPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.RPARAN, value: 0 }}
<<<<<<< HEAD
				${l.get_next_token()}	| ${{ type: EToken.EOF, value: 0 }}
			`('Lexer.get_next_token() - got $char, should return $expected', ({ char, expected }) => {
				expect(char).toEqual(expected);
			});
		});
	});

	describe('Floating point tests', () => {
		describe('2.5 / 1.25', () => {
			const l = new Lexer("2.5 / 1.25");
			test.each`
				char 					| expected
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 2.5 }}
				${l.get_next_token()}	| ${{ type: EToken.DIV, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 1.25 }}
				${l.get_next_token()}	| ${{ type: EToken.EOF, value: 0 }}
			`('Lexer.get_next_token() - got $char, should return $expected', ({ char, expected }) => {
				expect(char).toEqual(expected);
			});
		});
		describe('(260.25 - 123.15489) * 2.54', () => {
			const l = new Lexer("(260.25 - 123.15489) * 2.54");
			test.each`
				char 					| expected
				${l.get_next_token()}	| ${{ type: EToken.LPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 260.25 }}
				${l.get_next_token()}	| ${{ type: EToken.MINUS, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 123.15489 }}
				${l.get_next_token()}	| ${{ type: EToken.RPARAN, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.MUL, value: 0 }}
				${l.get_next_token()}	| ${{ type: EToken.INTEGER, value: 2.54 }}
				${l.get_next_token()}	| ${{ type: EToken.EOF, value: 0 }}
=======
>>>>>>> origin/UI
			`('Lexer.get_next_token() - got $char, should return $expected', ({ char, expected }) => {
				expect(char).toEqual(expected);
			});
		});
	});
<<<<<<< HEAD
=======
	test.todo('Add lexer tests / fix them based on desired behavior');
>>>>>>> origin/UI

	describe('Invalid values (error)', () => {
		test.each`
			str
			${"some invalid value"}
			${"twenty"}
<<<<<<< HEAD
			${"((1)2 3) 45"}
			${"42+-*/(22)45"}
			${"1   ()       +          2   - 45 ) 20"}
			${"1(2))3(4 5 6 ++ 7 // - 8)("}
			${"(20 + ) 30 - 20)"}
			${"((/ 123 * 150 051 + (97 5)*"}
		`('Lexer.get_next_token() with string "$str" should throw error at some point', (str) => {
			const l = new Lexer(str["str"]);
			expect(() => {
				for(let i = 0; i < str["str"].length; i++) {
					l.get_next_token();
				}
			}).toThrow();
		});
	});
});

describe('Parser tests', () => {
	describe('Arithmetic parser tests', () => {
		let eq = [
			// 1 + 2
			// (1 + 2)
			new BinOp(
				new Num(new Token(EToken.INTEGER, 1)), 
				new Token(EToken.PLUS), 
				new Num(new Token(EToken.INTEGER, 2))
			), 

			// (40 + 8) * 5
			new BinOp(
				new BinOp(
					new Num(new Token(EToken.INTEGER, 40)), 
					new Token(EToken.PLUS), 
					new Num(new Token(EToken.INTEGER, 8))
				), 
				new Token(EToken.MUL),
				new Num(new Token(EToken.INTEGER, 5))
			), 
		
			//(5 / 3) * 2 + 1
			new BinOp(
				new BinOp(
					new BinOp(
						new Num(new Token(EToken.INTEGER, 5)), 
						new Token(EToken.DIV), 
						new Num(new Token(EToken.INTEGER, 3))
					),
					new Token(EToken.MUL),
					new Num(new Token(EToken.INTEGER, 2))
				), 
				new Token(EToken.PLUS), 
				new Num(new Token(EToken.INTEGER, 1)))
			,
			// (((500 + 200) * (248 - 21)) * (42 + 81)) / 3 - 3
			new BinOp(
				new BinOp(
					new BinOp(
						new BinOp(
							new BinOp(
								new Num(new Token(EToken.INTEGER, 500)),
								new Token(EToken.PLUS),
								new Num(new Token(EToken.INTEGER, 200))
							),
							new Token(EToken.MUL),
							new BinOp(
								new Num(new Token(EToken.INTEGER, 248)),
								new Token(EToken.MINUS),
								new Num(new Token(EToken.INTEGER, 21))
							)
						),
						new Token(EToken.MUL),
						new BinOp(
							new Num(new Token(EToken.INTEGER, 42)),
							new Token(EToken.PLUS),
							new Num(new Token(EToken.INTEGER, 81))
						)
					),
					new Token(EToken.DIV),
					new Num(new Token(EToken.INTEGER, 3))
				),
				new Token(EToken.MINUS),
				new Num(new Token(EToken.INTEGER, 3))
			),
			// 100 - 7 * 5 / 3 * 2 + 500
			new BinOp(
				new BinOp(
					new Num(new Token(EToken.INTEGER, 100)),
					new Token(EToken.MINUS),
					new BinOp(
						new BinOp(
							new BinOp(
								new Num(new Token(EToken.INTEGER, 7)),
								new Token(EToken.MUL),
								new Num(new Token(EToken.INTEGER, 5))
							),
							new Token(EToken.DIV),
							new Num(new Token(EToken.INTEGER, 3))
						),
						new Token(EToken.MUL),
						new Num(new Token(EToken.INTEGER, 2))
					)
				), 
				new Token(EToken.PLUS),
				new Num(new Token(EToken.INTEGER, 500))
			)
		]
		test.each`
			str 													| expected
			${"1 + 2"}												| ${ eq[0] }
			${"(1 + 2)"}											| ${ eq[0] }
			${"(40 + 8) * 5"}										| ${ eq[1] }
			${"(5 / 3) * 2 + 1"} 									| ${ eq[2] }
			${"(((500 + 200) * (248 - 21)) * (42 + 81)) / 3 - 3"}	| ${ eq[3] }
			${"100 - 7 * 5 / 3 * 2 + 500"}							| ${ eq[4] }
		`('Parser.parse() with string "$str" should return $expected', ({ str, expected }) => {
			const l = new Lexer(str);
			const p = new Parser(l);
			expect(p.parse()).toEqual(expected);
=======
		`('Lexer.get_next_token() with string "$str" should throw error', (str) => {
			const l = new Lexer(str);
			expect(() => {
				l.get_next_token();
			}).toThrow();
		});
		test('Lexer.get_next_token() with string "123 abc" should throw error at some point', () => {
			const l = new Lexer("123 abc");
			expect(() => {
				l.get_next_token();
				l.get_next_token();
				l.get_next_token();
			}).toThrow();
>>>>>>> origin/UI
		});
	});
});

<<<<<<< HEAD
describe('Interpreter tests', () => {
	describe('Basic interpreter tests', () => {
		test.each`
			str								| expected
			${"1 + 2"}						| ${3}
			${"(1 * 2) + 6"}				| ${8}
			${"(40 + 2) * 2"}				| ${84}
			${"(1.25 + 1.25) / 2"}			| ${1.25}
			${"100 - 7 * 5 / 3 * 2 + 500"}	| ${565}
			${"(21 / 3) * (2 + 1)"}			| ${21}
			${"485.25 + 15.75"}				| ${501}
		`('Interpreter.interpret() with string "$str" should return $expected', ({ str, expected }) => {
			const l = new Lexer(str);
			const p = new Parser(l);
			const i = new Interpreter(p);
			expect(i.interpret()).toEqual(expected);
		});
	});
=======
describe('Parser tests', () => {
	test.todo('Parser tests');
>>>>>>> origin/UI
});