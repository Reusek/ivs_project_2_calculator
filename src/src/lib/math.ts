export interface IMathLib {
	/**
	 * Add the variables a and b.
	 * @param a number
	 * @param b number
	 * @returns number
	 */
	add(a: number, b: number): number;

	/**
	 * Multiplies the variable a and b.
	 * @param a number
	 * @param b number
	 * @returns multiplied numbers
	 */
	mul(a: number, b: number): number;

	/**
	 * Subtracts the variable b from a.
	 * @param a number
	 * @param b number
	 * @returns subtraction
	 */
	sub(a: number, b: number): number;

	/**
	 * Divide a by b
	 * @param a number
	 * @param b number
	 * @returns number
	 */
	div(a: number, b: number): number;

	/**
	 * N Factorial
	 * @throws Error
	 * @param n number
	 * @returns number
	 */
	fact(n: number): number;

	/**
	 * Power a^n
	 * @param a number
	 * @param n number
	 * @returns number
	 */
	power(a: number, n: number): number;

	/**
	 * An a root of n | n^1/a
	 * @param a number
	 * @param n number
	 * @returns number
	 */
	root(n: number): number;

	/**
	 * A square root of n
	 * @param n number
	 * @returns number
	 */
	sqrt(n: number): number;

	/**
	 * Modulo a and b
	 * @param a number
	 * @param b number
	 * @returns number
	 */
	mod(a: number, b: number): number;
}

class MathLib {
	static add(a: number, b: number) {
		return a + b;
	}

	static sub(a: number, b: number) {
		return a - b;
	}

	static mul(a: number, b: number) {
		return a * b;
	}

	static div(a: number, b: number) {
		if (b === 0) {
			throw Error();
		}

		return a / b;
	}

	static fact(n: number): number {
		if (n < 0) {
			throw Error();
		}

		if (n === 0) {
			return 1;
		}

		return n * this.fact(n - 1);
	}

	static power(a: number, n: number) {
		return a ** n;
	}

	static root(a: number, n: number) {
		return a ** this.div(1, n);
	}

	static sqrt(n: number) {
		return this.root(n, 2);
	}

	static mod(a: number, b: number) {
		if (b === 0) {
			throw Error();
		}

		return a % b;
	}
}

export default MathLib;
