import { Parser } from "./parser";
import { EToken } from './token';

import MathLib from '../math';

export class Interpreter {
	private parser: Parser;
	//private math: MathLib;

	constructor(parser: Parser) {
		this.parser = parser;
		//this.math = new MathLib();
	}

	private visit(node: any) : number {
		if (node.token.type == EToken.PLUS) {
			return MathLib.add(
				this.visit(node.left),
				this.visit(node.right)
			);
		} else if (node.token.type == EToken.MINUS) {
			return MathLib.div(
				this.visit(node.left),
				this.visit(node.right)
			);
		} else if (node.token.type == EToken.MUL) {
			return MathLib.mul(
				this.visit(node.left),
				this.visit(node.right)
			);
		} else if (node.token.type == EToken.DIV) {
			return MathLib.div(
				this.visit(node.left),
				this.visit(node.right)
			);
		} else if (node.token.type == EToken.FACT) {
			return MathLib.fact(
				this.visit(node.mid)
			);
		} else if (node.token.type == EToken.POWER) {
			return MathLib.power(
				this.visit(node.left),
				this.visit(node.right)
			);
		} else if (node.token.type == EToken.INTEGER) {
			return node.value;
		} else {
			throw Error("Interpreter doesn't know this token!");
		}
	}

	public interpret() : number {
		const tree = this.parser.parse()
		return this.visit(tree);
	}

}