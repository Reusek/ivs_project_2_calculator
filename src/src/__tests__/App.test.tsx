import '@testing-library/jest-dom';
import MathLib from '../lib/math';

/**
 * To run all the tests use:
 * 		yarn test
 *
 * To run specific tests use:
 * 		yarn test -t [keyword to match]
 *
 * 	example:	yarn test -t sub
 * 				- runs sub(a, b) tests
 *
 * 				yarn test -t pow
 * 				(or: yarn test -t power)
 * 				- runs power(a, b) tests
 *
 * (run yarn from ../ivs_project_2_calculator/src folder)
 *
 */

describe('Valid data MathLib tests', () => {
	/* add(a, b) tests */
	test.each`
		a              | b               | expected
		${0}           | ${0}            | ${0}
		${1}           | ${2}            | ${3}
		${-1}          | ${2}            | ${1}
		${1}           | ${-2}           | ${-1}
		${-100}        | ${-200}         | ${-300}
		${1.2}         | ${-1.2}         | ${0}
		${3.141}       | ${3.141}        | ${6.282}
		${1.000000001} | ${-1.000000001} | ${0}
	`('MathLib.add($a, $b) should return $expected', ({ a, b, expected }) => {
		expect(MathLib.add(a, b)).toBe(expected);
	});

	/* sub(a, b) tests */
	test.each`
		a              | b               | expected
		${0}           | ${0}            | ${0}
		${1}           | ${2}            | ${-1}
		${-1}          | ${2}            | ${-3}
		${1}           | ${-2}           | ${3}
		${-100}        | ${-200}         | ${100}
		${1.2}         | ${-1.2}         | ${2.4}
		${3.141}       | ${3.141}        | ${0}
		${1.000000001} | ${-1.000000001} | ${2.000000002}
	`('MathLib.sub($a, $b) should return $expected', ({ a, b, expected }) => {
		expect(MathLib.sub(a, b)).toBe(expected);
	});

	/* mul(a, b) tests */
	test.each`
		a        | b        | expected
		${0}     | ${0}     | ${0}
		${1}     | ${2}     | ${2}
		${-1}    | ${2}     | ${-2}
		${1}     | ${-2}    | ${-2}
		${-100}  | ${-200}  | ${20000}
		${1.2}   | ${-1.2}  | ${-1.44}
		${3.141} | ${3.141} | ${9.865881}
	`('MathLib.mul($a, $b) should return $expected', ({ a, b, expected }) => {
		expect(MathLib.mul(a, b)).toBe(expected);
	});

	/* div(a, b) tests */
	test.each`
		a       | b       | expected
		${1}    | ${2}    | ${0.5}
		${-1}   | ${2}    | ${-0.5}
		${1}    | ${-2}   | ${-0.5}
		${-100} | ${-200} | ${0.5}
		${1.2}  | ${-1.2} | ${-1}
		${60}   | ${8}    | ${7.5}
		${90}   | ${45}   | ${2}
	`('MathLib.div($a, $b) should return $expected', ({ a, b, expected }) => {
		expect(MathLib.div(a, b)).toBe(expected);
	});

	/* fact(a) tests */
	test.each`
		a     | expected
		${0}  | ${1}
		${1}  | ${1}
		${2}  | ${2}
		${10} | ${3628800}
	`('MathLib.fact($a) should return $expected', ({ a, expected }) => {
		expect(MathLib.fact(a)).toBe(expected);
	});

	/* power(a, b) tests */
	test.each`
		a      | b      | expected
		${0}   | ${0}   | ${1}
		${1}   | ${2}   | ${1}
		${-1}  | ${2}   | ${1}
		${4}   | ${-2}  | ${0.625}
		${2}   | ${10}  | ${1024}
		${4}   | ${2.5} | ${32}
		${-10} | ${5}   | ${-100000}
	`('MathLib.power($a, $b) should return $expected', ({ a, b, expected }) => {
		expect(MathLib.power(a, b)).toBe(expected);
	});

	/* sqrt(a, b) tests */
	test.each`
		a         | b    | expected
		${1}      | ${2} | ${1}
		${1024}   | ${2} | ${32}
		${27}     | ${3} | ${3}
		${10}     | ${0} | ${1}
		${390625} | ${8} | ${5}
	`('MathLib.sqrt($a, $b) should return $expected', ({ a, b, expected }) => {
		expect(MathLib.sqrt(a, b)).toBe(expected);
	});

	/* mod(a, b) tests */
	test.each`
		a        | b         | expected
		${4}     | ${2}      | ${0}
		${1}     | ${2}      | ${1}
		${-1}    | ${2}      | ${1}
		${40}    | ${50}     | ${40}
		${-100}  | ${-200}   | ${-100}
		${1.2}   | ${-1.2}   | ${0}
		${6.5}   | ${4}      | ${2.5}
		${1.258} | ${-0.256} | ${-0.022}
	`('MathLib.mod($a, $b) should return $expected', ({ a, b, expected }) => {
		expect(MathLib.mod(a, b)).toBe(expected);
	});
});

describe('Invalid data MathLib tests', () => {
	/* div(a, b) tests */
	test.each`
		a              | b
		${1}           | ${0}
		${-1}          | ${0}
		${48913}       | ${0}
		${-1.48941324} | ${0}
	`('MathLib.div($a, $b) should throw error', ({ a, b }) => {
		expect(() => {
			MathLib.div(a, b);
		}).toThrow();
	});

	/* fact(a) tests */
	test.each`
		a
		${-2}
		${1.5}
		${4.894248}
		${4000}
	`('MathLib.fact($a) should throw error', ({ a }) => {
		expect(() => {
			MathLib.fact(a);
		}).toThrow();
	});

	/* sqrt(a, b) tests */
	test.each`
		a               | b
		${10}           | ${0}
		${1024.1567489} | ${0}
		${0}            | ${0}
	`('MathLib.sqrt($a, $b) should throw error', ({ a, b }) => {
		expect(() => {
			MathLib.sqrt(a, b);
		}).toThrow();
	});

	/* mod(a, b) tests */
	test.each`
		a        | b
		${4}     | ${0}
		${1.258} | ${0}
	`('MathLib.mod($a, $b) should throw error', ({ a, b }) => {
		expect(() => {
			MathLib.mod(a, b);
		}).toThrow();
	});
});

describe('Precision MathLib tests', () => {
	/* add(a, b) tests */
	test.each`
		a                 | b                       | expected
		${0.123456}       | ${0.000005}             | ${0.123461}
		${0.999999999}    | ${0.000000001}          | ${1}
		${-0.99999999999} | ${-0.99999999999}       | ${-1.99999999998}
		${4294967296}     | ${18446744073709551616} | ${18446744078004518912}
	`('MathLib.add($a, $b) should return $expected', ({ a, b, expected }) => {
		expect(MathLib.add(a, b)).toBe(expected);
	});

	/* sub(a, b) tests */
	test.each`
		a                       | b                        | expected
		${0.123456789}          | ${0.0000000001}          | ${0.123456788}
		${-0.999999999999}      | ${0.999999999999}        | ${-1.999999999998}
		${0.999999999999}       | ${0.999999999999}        | ${0}
		${18446744073709551616} | ${-18446744073709551616} | ${0}
	`('MathLib.sub($a, $b) should return $expected', ({ a, b, expected }) => {
		expect(MathLib.sub(a, b)).toBe(expected);
	});

	/* mul(a, b) tests */
	test.each`
		a              | b               | expected
		${2.123456789} | ${2.123456789}  | ${4.509068734750190521}
		${1.5892}      | ${2.3}          | ${3.65516}
		${-1.951357}   | ${2}            | ${0.000001951357}
		${1}           | ${-2}           | ${-2}
		${-100}        | ${-200}         | ${20000}
		${1.2}         | ${-1.2}         | ${-1.44}
		${3.141}       | ${3.141}        | ${9.865881}
		${1.000000001} | ${-1.000000001} | ${-1.000000002000000001}
	`(
		'MathLib.mul($a, $b) should return (approximately at least) $expected',
		({ a, b, expected }) => {
			expect(MathLib.mul(a, b)).toBe(expected);
		}
	);

	/* div(a, b) tests */
	test.each`
		a          | b      | expected
		${9}       | ${11}  | ${0.81818181818181}
		${1}       | ${90}  | ${0.01111111111111}
		${0.84532} | ${-9}  | ${-0.09392444444444}
		${-100}    | ${452} | ${-0.22123893805309}
	`(
		'MathLib.div($a, $b) should return $expected...',
		({ a, b, expected }) => {
			// Using toBeCloseTo(), bcs idk how many decimal places -
			// - should I round the expected result to
			expect(MathLib.div(a, b)).toBeCloseTo(expected);
		}
	);

	/* fact(a) tests */
	test.each`
		a     | expected
		${15} | ${1307674368000}
		${18} | ${6402373705728000}
		${20} | ${121645100408832000}
	`('MathLib.fact($a) should return $expected', ({ a, expected }) => {
		expect(MathLib.fact(a)).toBe(expected);
	});

	/* power(a, b) tests */
	test.each`
		a     | b     | expected
		${2}  | ${52} | ${4503599627370496}
		${2}  | ${53} | ${9007199254740992}
		${25} | ${11} | ${2384185791015625}
	`('MathLib.power($a, $b) should return $expected', ({ a, b, expected }) => {
		expect(MathLib.power(a, b)).toBe(expected);
	});

	/* sqrt(a, b) tests */
	test.each`
		a     | b    | expected
		${2}  | ${2} | ${1.414213562373095}
		${10} | ${4} | ${1.778279410038922}
		${24} | ${6} | ${1.698381329564952}
	`('MathLib.sqrt($a, $b) should return $expected', ({ a, b, expected }) => {
		expect(MathLib.sqrt(a, b)).toBeCloseTo(expected);
	});

	/* mod(a, b) tests */
	test.each`
		a                   | b     | expected
		${1.123456789}      | ${-2} | ${-0.876543211}
		${4503599627370496} | ${43} | ${35}
	`('MathLib.mod($a, $b) should return $expected', ({ a, b, expected }) => {
		expect(MathLib.mod(a, b)).toBe(expected);
	});
});
