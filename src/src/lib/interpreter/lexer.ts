import { Token, EToken } from './token';


export default class Lexer
{
	/* Original string */
	private text: string;

	/* Current position on string */
	private pos: number;

	/* Current char on pos of text */
	private current_char: string;

	constructor(text: string) {
		this.text = text;
		this.pos = 0;
		this.current_char = this.text[this.pos];
	}

	/**
	 * Loads a new character and increment position.
	 * At the end of the original string returns empty string,
	 * witch represents end.
	 */
	private step() : void {
		this.pos += 1;
		if (this.pos > this.text.length - 1) {
			this.current_char = '';
		} else {
			this.current_char = this.text[this.pos];
		}
	}

	/**
	 * Skips all whitespace characters until
	 * nonwhitespace character.
	 */
	private skip_whitespace() : void {
		while (this.current_char !== '' && this.current_char === ' ') {
			this.step();
		}
	}

	/**
	 * Convert string to number.
	 * @returns converted string
	 */
	private integer() : number {
		let result = '';
		let is_float_set = false;
		while (this.current_char !== '' && (!isNaN(Number(this.current_char)) || this.current_char === '.')) {
			if (this.current_char === ' ') {
				this.skip_whitespace();
				continue;
			}

			if (this.current_char === '.' && !is_float_set) {
				is_float_set = true;
			} else if (this.current_char === '.' && !is_float_set) {
				throw SyntaxError('Číslo může obsahovat pouze jednu desetinou tečku!');
			}

			result += this.current_char;
			this.step();
		}

		return Number(result);
	}

	/**
	 * Break original string into tokens. One token at a time.
	 * @returns Token
	 */
	public get_next_token() : Token {
		while (this.current_char !== '') {
			if (this.current_char === ' ') {
				this.skip_whitespace();
				continue;
			}

			if (!isNaN(Number(this.current_char))) {
				return new Token(EToken.INTEGER, this.integer());
			}

			if (this.current_char === '+') {
				this.step();
				return new Token(EToken.PLUS);
			}

			if (this.current_char === '-') {
				this.step();
				return new Token(EToken.MINUS);
			}

			if (this.current_char === '*') {
				this.step();
				return new Token(EToken.MUL);
			}

			if (this.current_char === '/') {
				this.step();
				return new Token(EToken.DIV);
			}

			if (this.current_char === '(') {
				this.step();
				return new Token(EToken.LPARAN);
			}

			if (this.current_char === ')') {
				this.step();
				return new Token(EToken.RPARAN);
			}

			if (this.current_char === '!') {
				this.step();
				return new Token(EToken.FACT);
			}

			if (this.current_char === '^') {
				this.step();
				return new Token(EToken.POWER);
			}

			throw SyntaxError('Invalid syntax');
		}

		return new Token(EToken.EOF);
	}
}
