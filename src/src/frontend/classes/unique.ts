import { Action, onClickInterface } from "./action"
import { Operator } from "./operator"
import Lexer from "../../lib/interpreter/lexer"
import { Parser } from "../../lib/interpreter/parser"
import { Interpreter } from "../../lib/interpreter/interpreter"

export class PlaceHolder extends Action
{
	onClick(text: string): string
	{
		return text
	}
}

export class Result extends Action
{
	constructor(charCode: string, buttonName: string, render: boolean = true)
	{
		super(charCode, buttonName, "Vypočítat", render)
	}

	onClick(text: string): onClickInterface
	{
		let result = {} as onClickInterface;
		result.error = false

		console.log("Calculating...", text)

		try
		{
			var lexer = new Lexer(text)
			var parser = new Parser(lexer)
			var interpreter = new Interpreter(parser)

			result.text = String(interpreter.interpret())
		}
		catch (err)
		{
			console.error(err)
			result.error = true
			result.text = text
		}

		return result
	}
}

export class Backspace extends Action
{
	constructor(charCode: string, buttonName: string, render: boolean = true)
	{
		super(charCode, buttonName, "Vymazat znak", render)
	}

	onClick(text: string): string
	{
		if (text.length == 1)
		{
			return "0"
		}

		return text.slice(0, -1)
	}
}

export class Comma extends Operator
{
	constructor(charCode: string, buttonName: string, render: boolean = true)
	{
		super(charCode, buttonName, render)
	}

	canInsertFloatingPoint(text: string): boolean
	{
		var regexArray = text.match(/[+-]?\d+(?:\.\d+)?/g)

		if (regexArray![regexArray!.length - 1].includes("."))
		{
			return false
		}

		return true
	}

	onClick(text: string): string
	{
		if (this.canInsertOperator(text) && this.canInsertFloatingPoint(text))
		{
			return text + "."
		}

		return text
	}
}

export class Bracket extends Operator
{
	constructor(charCode: string, buttonName: string, render: boolean = true)
	{
		super(charCode, buttonName, render)
	}

	onClick(text: string): string
	{
		var lastChar = text.charAt(text.length - 1)

		if (this.charCode == ")")
		{
			var leftBracketCount = (text.match(/\(/g) || []).length
			var rightBracketCount = (text.match(/\)/g) || []).length

			// Zabraňuje odlišnému počtu pravých závorek jako levých závorek a výskytu prázdných závorek
			if (leftBracketCount == rightBracketCount || lastChar == "(")
			{
				return text
			}

			return text + this.charCode
		}
		
		// Zabraňuje vložení závorky přímo za číslo, je potřeba operátor mezi
		if (this.charCode == "(")
		{
			if (text == "0")
			{
				return this.charCode
			}

			if (!this.canInsertOperator(text))
			{
				return text + this.charCode
			}
		}

		return text
	}
}

export class Clear extends Action
{
	constructor(charCode: string, buttonName: string, render: boolean = true)
	{
		super(charCode, buttonName, "Vymazat vše", render)
	}

	onClick(): string
	{
		return "0"
	}
}

export class Factorial extends Operator
{
	constructor(charCode: string, buttonName: string, render: boolean = true)
	{
		super(charCode, buttonName, render)
	}

	isLastCharNumber(text: string): boolean
	{
		let lastChar = this.getLastChar(text)

		if (["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"].includes(lastChar))
		{
			return true
		}

		return false
	}

	onClick(text: string): string
	{
		if (text.length == 1 && text[0] == "0")
		{
			return this.charCode
		}

		if (this.getLastChar(text) == this.charCode || this.isLastCharNumber(text))
		{
			return text
		}

		return text + this.charCode
	}
}

export class Pow extends Operator
{
	constructor(charCode: string, buttonName: string, render: boolean = true)
	{
		super(charCode, buttonName, render)
	}

	onClick(text: string): string
	{
		if (this.canInsertOperator(text))
		{
			return text + "^"
		}

		return text
	}
}

export class Root extends Operator
{
	constructor(charCode: string, buttonName: string, render: boolean = true)
	{
		super(charCode, buttonName, render)
	}

	onClick(text: string): string
	{
		if (this.canInsertOperator(text))
		{
			return text + "^(1/"
		}

		return text
	}
}