import React from "react"

export interface TouchRippleInterface {
	start(): any,
	stop(event: Object, cb?: Function | undefined): any
}

export interface onClickInterface {
	text: string;
	error: boolean;
}

export abstract class Action {
	abstract onClick(text: string): onClickInterface | string;
	charCode: string;
	buttonName: string;
	name: string;
	shift: boolean = false;
	ctrl: boolean = false;
	render: boolean;
	ref: React.RefObject<TouchRippleInterface>;

	constructor(charCode: string, buttonName: string, name: string, render: boolean, ctrl: boolean = false, shift: boolean = false)
	{
		this.charCode = charCode
		
		if (shift)
		{
			this.shift = true;
		}

		if (ctrl)
		{
			this.ctrl = true;
		}

		this.buttonName = buttonName;
		this.name = name
		this.render = render
		this.ref = React.createRef()
	}

	isHeld(key: string, ctrlHeld: boolean, shiftHeld: boolean): boolean
	{
		if ((this.ctrl && !ctrlHeld) || (this.shift && !shiftHeld))
		{
			return false
		}

		return (this.charCode == key ? true : false)
	}

	getLastChar(text: string)
	{
		return text.charAt(text.length - 1)
	}
}