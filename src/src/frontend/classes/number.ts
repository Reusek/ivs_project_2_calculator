import { Action } from "./action"

export class Number extends Action
{
	constructor(charCode: string, buttonName: string, render: boolean)
	{
		super(charCode, buttonName, `Číslice ${charCode}`, render)
	}

	onClick(text: string): string
	{
		if (text == "0")
		{
			return this.charCode
		}

		var lastChar = this.getLastChar(text)

		// Zabraňuje, aby nebylo možné vložit číslo bezprostředně za závorku
		if (lastChar == ")")
		{
			return text
		}

		return text + this.charCode
	}
}

interface NumbersInterface {
	[key: string]: Number | undefined
}

export const Numbers: NumbersInterface = {
	"0": new Number("0", "0", true),
	"1": new Number("1", "1", true),
	"2": new Number("2", "2", true),
	"3": new Number("3", "3", true),
	"4": new Number("4", "4", true),
	"5": new Number("5", "5", true),
	"6": new Number("6", "6", true),
	"7": new Number("7", "7", true),
	"8": new Number("8", "8", true),
	"9": new Number("9", "9", true),
}