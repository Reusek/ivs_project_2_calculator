## Setup

Make sure you have installed Yarn.

```bash
npm install --g yarn
yarn
```

Install all dependencies:

```bash
yarn install
```

## Starting Development

**Make sure you are in the correct /src folder!**

Start the app in the `dev` environment:

```bash
yarn start
```

## Packaging for Production

Packaging the app:

```bash
yarn package
```

Packing the app for specific platform is done by using flag:

```bash
yarn package --[option]
#Example: yarn package --win
```
