import MathLib from "../../src/lib/math"

var fs = require('fs')

// Read until EOF
try {
  var data = fs.readFileSync(process.stdin.fd, 'utf-8')
} catch (e) {
  console.error('Error: Missing input')
  process.exit(1)
}

// Split and convert to array of integers
var numArr = data.split(/(?: |\n|\t)+/).map((x: string) => +x)
var numCount = numArr.length

// sum of input numbers (to calculate their average)
var sum = 0
// sum of input numbers squared
var xSum = 0

numArr.forEach((e: number) => {
  sum = MathLib.add(sum, e) // sum += e
  xSum = MathLib.add(xSum, MathLib.mul(e, e)) // xSum += e^2
})

// _x
var xAvg = MathLib.div(sum, numCount)
// sum - N * _x^2
xSum = MathLib.sub(xSum, MathLib.mul(MathLib.mul(xAvg, xAvg), numCount))

// sqrt( (xSum)/(N-1) )
var sDevi = MathLib.root(MathLib.div(xSum, MathLib.sub(numCount, 1)), 2)

console.log(sDevi)
