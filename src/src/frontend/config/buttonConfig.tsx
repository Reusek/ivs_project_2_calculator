import { Result, Backspace, Comma, Bracket, Clear, Factorial, PlaceHolder, Pow, Root } from "../classes/unique"
import { Numbers } from "../classes/number"
import { Operators } from "../classes/operator"

export const UniqueActions = {
	",": new Comma(",", ",", false),
	".": new Comma(".", "."),
	"Backspace": new Backspace("Backspace", "⌫"),
	"Clear": new Clear("Delete", "CE"),
	"=": new Result("=", "="),
	"Enter": new Result("Enter", "=", false),
	"(": new Bracket("(", "("),
	")": new Bracket(")", ")"),
	"!": new Factorial("!", "!"),
	"null": new PlaceHolder("", "null", "null", true),
	"^": new Pow("š", "x<sup>y</sup>"),
	"√": new Root("√", "√")
}

export const buttons = [
	UniqueActions["("],
	UniqueActions[")"],
	UniqueActions["Backspace"],
	Operators["%"],
	UniqueActions["!"],
	UniqueActions["^"],
	UniqueActions["√"],
	Operators["+"],
	Numbers["7"],
	Numbers["8"],
	Numbers["9"],
	Operators["-"],
	Numbers["4"],
	Numbers["5"],
	Numbers["6"],
	Operators["*"],
	Numbers["1"],
	Numbers["2"],
	Numbers["3"],
	Operators["/"],
	UniqueActions["Clear"],
	Numbers["0"],
	UniqueActions["."],
	UniqueActions[","],
	UniqueActions["="],
	UniqueActions["Enter"],
]