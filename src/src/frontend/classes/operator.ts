import { Action } from "./action"

export class Operator extends Action
{
	constructor(charCode: string, buttonName: string, render: boolean = true)
	{
		super(charCode, buttonName, `Operátor ${charCode}`, render)
	}

	canInsertOperator(text: string): boolean
	{
		let lastChar = this.getLastChar(text)

		// Operátor vložíme kdykoli, pokud předchozí znak je číslo nebo závorka, protože dva operátory za sebou nejsou validní..
		// Závorky kontrolujeme taky, abychom mohli vkládat operátory přímo za závorku
		if (["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "(", ")"].includes(lastChar))
		{
			return true
		}

		return false
	}

	isLastSpecialCharacter(text: string): boolean
	{
		let lastChar = this.getLastChar(text)

		if (["!", "^"].includes(lastChar))
		{
			return true
		}

		return false
	}

	onClick(text: string): string
	{
		if (this.canInsertOperator(text) && !this.isLastSpecialCharacter(text))
		{
			return text + this.charCode
		}

		return text
	}
}

interface OperatorsInterface {
	[key: string]: Operator | undefined
}

export const Operators: OperatorsInterface = {
	"+": new Operator("+", "+"),
	"-": new Operator("-", "-"),
	"*": new Operator("*", "*"),
	"/": new Operator("/", "/"),
	"%": new Operator("%", "%")
}