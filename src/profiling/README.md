

# PKG

TypeScript starter template for creating a Node.js package.

It uses TypeScript compiler to build modules and compiles the entire package into a single executable file.


## Getting Started

 1.  Install dev dependencies with `npm i`

## Usage

Run the compiler in watch mode:
````
npm run dev
````

Build the project:
````
npm run build
````

Prepare before publishing:
````
npm run prepare
````