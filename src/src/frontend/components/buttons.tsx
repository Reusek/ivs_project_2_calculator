import React from "react";
import { Button, withStyles } from '@material-ui/core';
import TouchRipple from "@material-ui/core/ButtonBase/TouchRipple";
import grey from '@material-ui/core/colors/grey';
import { buttons } from "../config/buttonConfig"

const useStyles = () => ({
	button: {
		backgroundColor: grey[200]
	}
});

class Buttons extends React.Component<any, any> {
	buttons: Array<any>

	constructor(props: any)
	{
		super(props)

		this.buttons = buttons
	}

	render()
	{
		const { classes, text, updateState } = this.props;

		return (
			<>
				{this.buttons.map((button) => {
					if (button?.render)
					{
						return (
							<div className="col-3" key={button.name}>
								<Button
									className="w-100 p-3"
									variant="contained"
									color="primary"
									classes={{root: classes.button}}
									aria-label={button.name}
									disableElevation
									onClick={() => { updateState(button.onClick(text))}}
								>
									<span dangerouslySetInnerHTML={{__html: button.buttonName}}></span>
									<TouchRipple ref={button.ref}/>
								</Button>								
							</div>
						)
					}
				})}
			</>
		)
	}
}

export default withStyles(useStyles)(Buttons)