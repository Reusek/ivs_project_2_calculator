import React from "react";
import './scss/App.global.scss';
import { Paper, createMuiTheme, ThemeProvider, Snackbar, Backdrop } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import Buttons from "./components/buttons"
import { buttons } from "./config/buttonConfig"
import { onClickInterface, TouchRippleInterface } from "./classes/action"

const theme = createMuiTheme({
    palette: {
        primary: {
            main: "#e3e3e3",
        },
    },
});

class App extends React.Component<any, { text: string, render: string, error: boolean, help: boolean }> {
	private focusDiv = React.createRef<HTMLElement>();
	private contentRef = React.createRef<HTMLElement>();

	constructor(props: any) {
		super(props);

		this.state = {
			text: "0",
			render: "0",
			error: false,
			help: false
		}

		this.focusDiv = React.createRef<HTMLElement>();
		this.contentRef = React.createRef<HTMLElement>();
		this.changeState = this.changeState.bind(this);
		this.onKeybordEvent = this.onKeybordEvent.bind(this);
		this.onBlur = this.onBlur.bind(this);
		this.triggerRipple = this.triggerRipple.bind(this);
		this.parseRender = this.parseRender.bind(this);
	}

	componentDidMount() {
		// Keyboard inputy se nesnímají, pokud není daný prvek focusnut, ? operator se stará o to, aby nedošlo k chybě, pokud: this.focusDiv == null || this.focusDiv.current == null
		this.focusDiv?.current?.focus();
	}

	// Emulace ripple efektu
	triggerRipple(ref: TouchRippleInterface | null | undefined) {
		ref?.start();
		setTimeout(() => {
			ref?.stop({});
		}, 100);
	};

	parseRender(text: string): string {
		var numbers = text.match(/\d+(?:\.\d+)?/g) || [] // Regex rozděluje čísla včetně desetinného čísla, bez znaménka
		console.log(numbers)

		text = text.replaceAll("/", "<span class='operator'>/</span>")
					.replaceAll("-", "<span class='operator'>-</span>")
					.replaceAll("*", "<span class='operator'>*</span>")
					.replaceAll("+", "<span class='operator'>+</span>")
					.replaceAll("%", "<span class='operator'>+</span>")
					.replaceAll("!", "<span class='operator'>fact</span>")

		return text
	}

	changeState(input: onClickInterface | string)
	{
		var textContainer = this.contentRef.current
		var newRender = ""
		var newText = ""
		var error = false

		if (typeof input == "string")
		{
			// Pokud se nic nestalo, neměníme stav -> nedojde k rerenderu
			if (input == this.state.text)
			{
				return;
			}

			textContainer?.classList.remove("error-border")
			textContainer?.classList.remove("success-border")
			newRender = this.parseRender(input)
			newText = input
		}
		else
		{
			if (input.error)
			{
				textContainer?.classList.add("error-border")
				textContainer?.classList.remove("success-border")
			}
			else
			{
				textContainer?.classList.add("success-border")
				textContainer?.classList.remove("error-border")
			}

			newRender = this.parseRender(input.text)
			newText = input.text
			error = input.error
		}

		this.setState((current) => ({ ...current, text: newText, render: newRender, error: error}))
	}

	onKeybordEvent(e: React.KeyboardEvent)
	{
		console.log(e.key)

		// Iterujeme skrze všechny operace a zjistíme, zda se Keyboard event nějaké rovná, pokud by to tu nebylo, neměli bychom jak limitovat písmenka apod.
		buttons.some((button) => {
			if (button?.charCode === e.key)
			{
				// Zavoláme ripple effekt daného tlačítka, který se projevuje pouze při kliknutí -> konzistence UI
				for (var i = 0; i < buttons.length; i++)
				{
					if (buttons[i]?.charCode == button.charCode && buttons[i]?.ref.current != null)
					{
						this.triggerRipple(buttons[i]?.ref.current)
						break // Našli jsme tlačítko v UI, můžeme break
					}
				}
				// Čudlíky při klikání a keyboard inputy by měly dělat tu stejnou věc, zavoláme stejnou funkci...
				this.changeState(button.onClick(this.state.text))
				return true // Našli jsme správný čudlík, můžeme exitovat loop
			}

			return false
		});
	}

	onBlur(e: React.FocusEvent)
	{
		console.log(e)

		this.focusDiv?.current?.focus();
	}

	render() {
		// Pomůčka pro přehlednější práci se stavy
		const { text, render, error, help } = this.state;
		console.log(this.state)

		// Kompletní render, výstup z .map by případně šel převést do proměnné pro lepší přehlednost
		return (
			<ThemeProvider theme={theme}>
				<main
					ref={this.focusDiv}
					className="w-100 p-3"
					tabIndex={-1}
					onBlur={this.onBlur}
					onKeyDown={this.onKeybordEvent}
					role="presentation"
				>
					<div className="w-100 text-end mb-2">
						<Paper ref={this.contentRef} className="p-3" elevation={3} dangerouslySetInnerHTML={{__html: render}}/>
					</div>
					<div className="row g-1">
						<Buttons updateState={this.changeState} text={text}/>
					</div>
					<div className="mt-3 d-flex justify-content-center align-items-center">
						<i onClick={() => {this.setState((current) => ({ ...current, help: true}))}}
						   className="question-circle">?</i>
					</div>
				</main>
				<Snackbar open={error} autoHideDuration={5000} onClose={() => {this.setState((current) => ({ ...current, error: false}))}}>
					<Alert elevation={6} variant="filled" onClose={() => {this.setState((current) => ({ ...current, error: false}))}} severity="error">
					Nastala chyba při výpočtu, zkontrolujte, prosím, vzorec.
					</Alert>
				</Snackbar>
				<Backdrop style={{zIndex: 99999, backgroundColor: "rgba(0, 0, 0, 0.7)"}} open={help} onClick={() => {this.setState((current) => ({ ...current, help: false}))}}>
					<div className="text-center container-sm" style={{color: "white"}}>
						<h1>Nápověda</h1>
						<p>Kalkulačka nabízí grafické rozhrání, které se dá ovládat jak myší, tak pomocí klávesnice.<br/>Číslice pomocí numerické klávesnice, případně
						kombinací Shift + čísla na klasické klávesnici. Operace se provádějí podle daného znaku na tlačítku.</p>
						<p><b>Pokročilé klávesové zkrátky:</b></p>
						<ul className="mb-3">
							<li>DELETE - Smazat celý vzorec.</li>
						</ul>
						<p><b>Pokročilé matematické operace:</b></p>
						<ul>
							<li>! - Faktoriál. Číslo umístěte za označení "fact": <b>fact 45</b></li>
							<li>^ - Mocnění. Číslo umístěte před označení "^"<br/> a exponent za označení: <b>2^5</b></li>
							<li>√ - Odmocnění. Číslo umístěte před označení "√",<br/> odmocnitel za zlomek "1/" a ukončete závorku: <b>4√(1/2)</b></li>
						</ul>
					</div>
				</Backdrop>
			</ThemeProvider>
		);
	}
}

export default App