# IVS Projekt 2 - calculator

## Prostředí

Linux 64 bit (Debian/Ubuntu)
Windows 64 bit

## Autoři

- xslova22 Michal Slovák
- xmachy02 Augustin Machyňák
- xklink01 Albert Klinkovský

## Licence

The source code for the site is licensed under the MIT license.